# Tic Tac Toe
Créer un Tic Tac Toe... (Morpion)

## Règles du jeu :
Sur un plateau de 9 cases (3 par 3)

Tenter d'aligner 3 de ses pions (X ou O) dans la largeur, hauteur ou en diagonale

Empêcher son adversaire de faire de même

Le premier qui arrive à aligner 3 de ses pions a gagné

Chaque joueur joue l'un après l'autre

## Version 1 : 2 joueurs 
Créer un Tic Tac Toe jouable à 2 en Javascript, HTML et CSS

La grille doit utiliser bootstrap pour s'afficher (elle fait toujours 3 colonnes, peu importe la taille de l'écran, mais les images doivent s'adapter)

Chaque case affiche :
- soit une case vide (trasparente)
- soit une croix
- soit un rond

Chaque case est de la même taille en hauteur et largeur

## Version 2 : 1 à 2 joueurs
Donner la possibilité de jouer seul face à l'ordinateur... ou à deux

## Version Améliorée 
- Changer la taille de la grille dynamiquement (attention à la grille bootstrap, passer à autre chose (variables CSS par exemple))
- Utiliser un sprite pour afficher les pions (https://www.alsacreations.com/tuto/lire/1068-sprites-css-background-position.html)
- Donner la possibilité de choisir son pion et le plateau


