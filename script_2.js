// constructeur de btn case
class Btn {
    constructor(str) {
        this.target = document.getElementById(str); // element du dom
        this.state = 0; // etat du bouton pour checker les conditions 
        this.srcList = ["./TicTacToe/empty.png", "./TicTacToe/cross-2.png", "./TicTacToe/circle-2.png"]
    }
}
// constructeur de player
class Player {
    constructor() {
        this.turn = document.getElementById('turn')
        this.state = true;
        this.msg = ["Player 1 go", "Player 2 ... ba allez go"]
    }
}
var endGame = false;
// fonction principale qui change le tour du joueur 
function main(btn_n, int) {
    if (btn_n.state == 0 && player.state == true) {
        player.turn.innerText = player.msg[0]
        btn_n.state = 1
        btn_n.target.src = btn_n.srcList[1]
        player.state = false;
        iaT[int-1] = 1
    } else if (btn_n.state == 0 && player.state != true) {
        player.turn.innerText = player.msg[1]
        btn_n.state = 2
        btn_n.target.src = btn_n.srcList[2]
        player.state = true;
    } else {
        alert('WOW Calm the fuck down')
    }
}
// fonction qui teste les btn.state
function win(btnA, btnB, btnC) {
    // si ce n'est pas la fin du jeu
    if (endGame != true) {
        if (btnA.state == 1 && btnB.state == 1 && btnC.state == 1) {
            alert("Joueur 1 Gj")
            endGame = true;
            clearTimeout(timer)
        } else if (btnA.state == 2 && btnB.state == 2 && btnC.state == 2) {
            alert("Joueur 2 Ba voila quand tu veux")
            endGame = true;
            clearTimeout(timer)
        } else if (btn_1.state != 0 && btn_2.state != 0 && btn_3.state != 0 && btn_4.state != 0 && btn_5.state != 0 && btn_6.state != 0 && btn_7.state != 0 && btn_8.state != 0 && btn_9.state != 0) {
            alert('Tie')
            endGame = true;
            clearTimeout(timer)
        }
    }
}
// fonction qui check row column & diag
function checkWin() {
    checkRow();
    checkColumn();
    checkDiag();
}
// fonction qui check les row
function checkRow() {
    win(btn_1, btn_2, btn_3)
    win(btn_4, btn_5, btn_6)
    win(btn_7, btn_8, btn_9)
}
// fonction qui check les column
function checkColumn() {
    win(btn_1, btn_4, btn_7)
    win(btn_2, btn_5, btn_8)
    win(btn_3, btn_6, btn_9)
}
// fonction qui check les diagonales
function checkDiag() {
    win(btn_1, btn_5, btn_9)
    win(btn_3, btn_5, btn_7)
}
function checkWinIa() {
    checkRowIa();
    checkColumnIa();
    checkDiagIa();
}
// fonction qui check les row de l'ia
function checkRowIa() {
    win(iaT[0], iaT[1], iaT[2])
    win(iaT[3], iaT[4], iaT[5])
    win(iaT[6], iaT[7], iaT[8])
}
// fonction qui check les column de l'ia
function checkColumnIa() {
    win(iaT[0], iaT[3], iaT[6])
    win(iaT[1], iaT[4], iaT[7])
    win(iaT[2], iaT[5], iaT[8])
}
// fonction qui check les diagonales de l'ia
function checkDiagIa() {
    win(iaT[0], iaT[4], iaT[8])
    win(iaT[2], iaT[4], iaT[6])
}
// création d'un objet Player
var player = new Player()
// instancie les 9 btn et ajoute l'evenement onclick
for (let index = 1; index < 10; index++) {
    window[`btn_${index}`] = new Btn(`${index}`);
    window[`btn_${index}`].target.onclick = () => {
        if (gameMode.state == 0) {
            main(window[`btn_${index}`])
            checkWin();
        } else {
            main(window[`btn_${index}`], index)
            checkWin();
            timerIa();            
        }
    }
}
function timerIa() {
    timer = setTimeout(ia, 1000)
}
var gameMode = new Btn('mode')
gameMode.target.onclick = () => {
    if (gameMode.state == 0) {
        gameMode.target.innerHTML = "VS Bot"
        gameMode.state = 1
    } else {
        gameMode.target.innerHTML = "VS Joueur"
        gameMode.state = 0
    }
}
function rand () {
    var x = Math.floor(Math.random()*9)
    return x
}
var iaT = [btn_1, btn_2, btn_3, btn_4, btn_5, btn_6, btn_7, btn_8, btn_9]
function ia () {
    var r = rand()
    if(iaT[r].state == 0){
        iaT[r].state = 2
        window[`btn_${r+1}`].state = 2
        window[`btn_${r+1}`].target.src = btn_1.srcList[2]
        player.turn.innerText = player.msg[1]
        player.state = true
        checkWinIa();
    } else  {
        ia()
    }
}